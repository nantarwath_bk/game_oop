package Cilen_game;
import java.io.Serializable;

public class dataXY implements Serializable{

	private static final long serialVersionUID = 1L;
	int[] x = new int[4];
	int[] y = new int[4];
	int x1,point1;
	int y1;
	int x2,point2;
	public int getPoint1() {
		return point1;
	}
	public void setPoint1(int point1) {
		this.point1 = point1;
	}
	public int getPoint2() {
		return point2;
	}
	public void setPoint2(int point2) {
		this.point2 = point2;
	}
	int y2;
	int x3;
	int y3;
	int x4;
	int y4;
	int chackXY;
	String ip1,ip2,Name1,Name2;
	public String getName1() {
		return Name1;
	}
	public void setName1(String Name1) {
		this.Name1 = Name1;
	}
	public String getName2() {
		return Name2;
	}
	public void setName2(String Name2) {
		this.Name2 = Name2;
	}
	public String getIp1() {
		return ip1;
	}
	public void setIp1(String ip1) {
		this.ip1 = ip1;
	}
	public String getIp2() {
		return ip2;
	}
	public void setIp2(String ip2) {
		this.ip2 = ip2;
	}
	public int getX2() {
		return x2;
	}
	public void setX2(int x2) {
		this.x2 = x2;
	}
	public int getY2() {
		return y2;
	}
	public void setY2(int y2) {
		this.y2 = y2;
	}
	public int getX3() {
		return x3;
	}
	public void setX3(int x3) {
		this.x3 = x3;
	}
	public int getY3() {
		return y3;
	}
	public void setY3(int y3) {
		this.y3 = y3;
	}
	public int getX4() {
		return x4;
	}
	public void setX4(int x4) {
		this.x4 = x4;
	}
	public int getY4() {
		return y4;
	}
	public void setY4(int y4) {
		this.y4 = y4;
	}
	public int getX1() {
		return x1;
	}
	public void setX1(int x1) {
		this.x1 = x1;
	}
	public int getY1() {
		return y1;
	}
	public void setY1(int y1) {
		this.y1 = y1;
	}
	public int getChackXY() {
		return chackXY;
	}
	public void setChackXY(int chackXY) {
		this.chackXY = chackXY;
	}
	public int[] getX() {
		return x;
	}
	public void setX(int[] x) {
		this.x = x;
		//System.out.println("data X:"+x);
	}
	public int[] getY() {
		return y;
	}
	public void setY(int[] y) {
		this.y = y;
		//System.out.println("data Y:"+y);
	}

}
