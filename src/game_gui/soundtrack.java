package game_gui;

import java.io.File;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

import org.omg.CORBA.PUBLIC_MEMBER;

public class soundtrack {

	public static void main(String[] args) {
		Thread t1=new Thread(new MyThread());
		t1.start();
	}
}
class MyThread extends Thread {
	public void run() {
		while (true) {
			try {
				File yourFile;
				AudioInputStream stream;
				AudioFormat format;
				DataLine.Info info;
				Clip clip;
				File soundtrack=new File(System.getProperty("user.dir")+ File.separator+"Summer.wav");
				
				stream = AudioSystem.getAudioInputStream(soundtrack);
				format = stream.getFormat();
				info = new DataLine.Info(Clip.class,format );
				clip = (Clip) AudioSystem.getLine(info);
				clip.open(stream);
				clip.start();
				
				Thread.sleep(100000);
				
			} catch (Exception e) {
				System.out.print(e.getMessage());
				
			}
			
		}
	}

	
}
