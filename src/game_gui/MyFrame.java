package game_gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.net.InetAddress;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Cilen_game.ALL_cilent;
import Cilen_game.RUNN;
import Cilen_game.dataXY;

public class MyFrame extends JFrame{
	
	private JPanel contentPane;
	JPanel panel=new JPanel();
	JLabel Background=new JLabel("");
	JButton button_Start = new JButton("");
	JButton button_Exit = new JButton("");
	JButton button_Developer=new JButton("");
	JButton button_Howto=new JButton("");
	dataXY dt = new dataXY();
	public String ipserver;
	public String input;
	public MyFrame(){
		

/*		ipserver = JOptionPane.showInputDialog(this, "input IP!!");
		String[] choices = { "play 1", "play 2"};
		input = (String) JOptionPane.showInputDialog(null, "Choose now...",
			        "The Choice of a Lifetime", JOptionPane.QUESTION_MESSAGE, null,
			        choices,choices[1]);
		if(input.equals(choices[0])) {
			input="1";
			dataxy.setName1(input);
		}else if(input.equals(choices[1])) {
			input="2";
			dataxy.setName2(input);
		}*/
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1290, 822);
		setLocationRelativeTo(null);
		setLayout(null);
		contentPane = new JPanel();
		setContentPane(contentPane);
		Background.setIcon(new ImageIcon(MyFrame.class.getResource("/image/BG_Home.png")));
		
		button_Start.setSize(176, 51);
		button_Start.setLocation(30, 510);
		button_Start.setIcon(new ImageIcon(MyFrame.class.getResource("/image/button_Start.png")));
		button_Start.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				CharecterFrame charFrame=new CharecterFrame();
				charFrame.setVisible(true);
				setVisible(false);
				
			}
		});
		
		button_Howto.setSize(260, 51);
		button_Howto.setLocation(30, 570);
		button_Howto.setIcon(new ImageIcon(MyFrame.class.getResource("/image/button_howto.png")));
		button_Howto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				HowtoPlay HowtoFrame=new HowtoPlay();
				HowtoFrame.setVisible(true);
				setVisible(false);
				
			}
		});		
		
		button_Developer.setSize(222, 51);
		button_Developer.setLocation(30, 630);
		button_Developer.setIcon(new ImageIcon(MyFrame.class.getResource("/image/button_Developer.png")));
		button_Developer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Developer developer = new Developer();
				developer.setVisible(true);
				setVisible(false);
				
			}
		});
	
		button_Exit.setSize(176, 51);
		button_Exit.setLocation(30, 690);
		button_Exit.setIcon(new ImageIcon(MyFrame.class.getResource("/image/button_Exit.png")));
		button_Exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Exit");
				System.exit(0);			
			}
		});
		
		contentPane.add(Background);
		Background.add(button_Start);
		Background.add(button_Exit);
		Background.add(button_Developer);
		Background.add(button_Howto);
		
			
	}
	

}
