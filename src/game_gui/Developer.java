package game_gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Developer extends JFrame {
	
	private JPanel contentPane;
	JPanel panel=new JPanel();
	JLabel Background=new JLabel("");
	JButton button_Back = new JButton("");
	
	public Developer(){
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1290, 822);
		setLocationRelativeTo(null);
		setLayout(null);
		contentPane = new JPanel();
		setContentPane(contentPane);
		Background.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Developer.png")));
		
		button_Back.setSize(176, 51);
		button_Back.setLocation(30, 8);
		button_Back.setIcon(new ImageIcon(MyFrame.class.getResource("/image/button_Back.png")));
		button_Back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				MyFrame frame = new MyFrame();
				frame.setVisible(true);	
				setVisible(false);
				
			}
		});
		
		contentPane.add(Background);
		Background.add(button_Back);
		
	}

}
