package game_gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Cilen_game.main_cilent;

public class CharecterFrame extends JFrame {
	
	private JPanel contentPane;
	JPanel panel=new JPanel();
	JLabel label_pick=new JLabel();
	JLabel Background=new JLabel("");
	JButton button_OK = new JButton("");
	JButton button_Back = new JButton("");
	
	JButton button_char1=new JButton();
	JButton button_char2=new JButton();
	JButton button_char3=new JButton();
	JButton button_char4=new JButton();
	JButton button_char5=new JButton();
	JButton button_char6=new JButton();
	
	JLabel ellie=new JLabel();
	JLabel zoe=new JLabel();
	JLabel meade=new JLabel();
	JLabel deanu=new JLabel();
	JLabel lobster=new JLabel();
	JLabel mickey=new JLabel();
	
	main_cilent gui1;
	
	public CharecterFrame(){
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1290, 822);
		setLocationRelativeTo(null);
		setLayout(null);
		contentPane = new JPanel();
		setContentPane(contentPane);
		Background.setIcon(new ImageIcon(MyFrame.class.getResource("/image/BG_Char.jpg")));
		
		button_OK.setSize(174, 53);
		button_OK.setLocation(1070, 700);
		button_OK.setIcon(new ImageIcon(MyFrame.class.getResource("/image/button_Ready.png")));
		button_OK.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				gui1 = new main_cilent();
				//gui1.setVisible(true);
				setVisible(false);
				
			}
		});
		
		button_Back.setSize(176, 51);
		button_Back.setLocation(30, 700);
		button_Back.setIcon(new ImageIcon(MyFrame.class.getResource("/image/button_Back.png")));
		button_Back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				MyFrame frame = new MyFrame();
				frame.setVisible(true);	
				setVisible(false);
				
			}
		});
		
		label_pick.setSize(500, 200);
		label_pick.setLocation(450, -40);
		label_pick.setIcon(new ImageIcon(MyFrame.class.getResource("/image/label_pick.png")));
		
		button_char1.setSize(210, 248);
		button_char1.setLocation(100, 120);
		button_char1.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Character1.png")));
		
		ellie.setSize(192, 192);
		ellie.setLocation(110, 125);
		ellie.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Ellie1.gif")));
		
		button_char2.setSize(210, 248);
		button_char2.setLocation(500, 120);
		button_char2.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Character2.png")));
		
		
		zoe.setSize(192, 192);
		zoe.setLocation(520, 125);
		zoe.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Zoe2.gif")));
		
		
		button_char3.setSize(210, 248);
		button_char3.setLocation(900, 120);
		button_char3.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Character3.png")));
		
		
		meade.setSize(192, 192);
		meade.setLocation(920, 125);
		meade.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Meade3.gif")));
		
		
		button_char4.setSize(210, 248);
		button_char4.setLocation(100, 420);
		button_char4.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Character4.png")));
		
		deanu.setSize(192, 192);
		deanu.setLocation(120, 420);
		deanu.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Deanu4.gif")));
		
		
		button_char5.setSize(210, 248);
		button_char5.setLocation(500, 420);
		button_char5.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Character5.png")));
		
		
		lobster.setSize(192, 192);
		lobster.setLocation(520, 423);
		lobster.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Lobster5.gif")));
		
		button_char6.setSize(210, 248);
		button_char6.setLocation(900, 420);
		button_char6.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Character6.png")));
		
		
		mickey.setSize(192, 192);
		mickey.setLocation(910, 423);
		mickey.setIcon(new ImageIcon(MyFrame.class.getResource("/image/Mickey6.gif")));
		
		
		
		
		contentPane.add(Background);
		Background.add(label_pick);
		Background.add(button_OK);
		Background.add(button_Back);
		
		Background.add(ellie);
		Background.add(button_char1);
		
		Background.add(zoe);
		Background.add(button_char2);
		
		Background.add(meade);
		Background.add(button_char3);
		
		Background.add(deanu);
		Background.add(button_char4);
		
		Background.add(lobster);
		Background.add(button_char5);
		
		Background.add(mickey);
		Background.add(button_char6);
		
	}
}


